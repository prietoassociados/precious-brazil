<footer>

    <div class="footer -first">

        <div class="footer-container container">

            <div class="footer-holder row">

                <div class="footer-logos col-lg-8">

                    <div class="logos row align-items-end">

                        <a class="link col-lg-3 col-md-4" href="<?php bloginfo('url') ?>" title="Precious Brazil">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-preciousbrazil.svg" />
                        </a>

                        <a class="link col-lg-3 col-md-4" href="http://www.ibgm.com.br" title="IBGM" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-ibgm.svg" />
                        </a>

                        <a class="link col-lg-3 col-md-4" href="http://www.apexbrasil.com.br" title="Apex Brasil" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-apex.svg" />
                        </a>

                    </div>

                </div>

                <div class="footer-contacts col-lg-4">
                    <p><?php _e('Fone:', 'ps'); ?> <a href="tel:556133263926">+55 61 3326-3926</a></p>
                    <p>E-mail: <a href="mailto:preciousbrazil@ibgm.com.br" target="_blank">preciousbrazil@ibgm.com.br</a></p>
                </div>

            </div>

        </div>

    </div>

    <div class="footer -second">

        <div class="footer-container container">

            <div class="footer-holder row">

                <div class="footer-menu col-md-3">

                    <h3 class="title"><?php _e('Acesso rápido', 'ps'); ?></h3>

                    <nav class="menu">
                        <a href="<?php bloginfo('url') ?>/<?php _e('quem-somos', 'ps'); ?>"><?php _e('Quem somos', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('acoes', 'ps'); ?>"><?php _e('Ações', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('empresas', 'ps'); ?>"><?php _e('Empresas', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('agenda', 'ps'); ?>"><?php _e('Agenda', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('noticias', 'ps'); ?>"><?php _e('Noticias', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('imprensa', 'ps'); ?>"><?php _e('Imprensa', 'ps'); ?></a>
                        <a href="<?php bloginfo('url') ?>/<?php _e('contato', 'ps'); ?>"><?php _e('Contato', 'ps'); ?></a>
                    </nav>

                </div>

                <div class="footer-menu col-md-3">

                    <h3 class="title"><?php _e('Redes sociais', 'ps'); ?></h3>

                    <nav class="menu">
                        <a href="https://www.facebook.com/ibgm.brasil/" target="_blank">Facebook</a>
                        <a href="https://www.instagram.com/ibgmbrasil/?hl=pt-br" target="_blank">Instagram</a>
                    </nav>

                </div>

                <div class="footer-endereco col-md-6">

                    <div class="endereco">

                        <div class="endereco-holder  d-flex align-items-end">

                            <div class="image">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/ico-endereco-w.svg" alt="" />
                            </div>

                            <div class="content">
                                <h3 class="title"><?php _e('Endereço', 'ps'); ?></h3>
                                <p>
                                    SNH, QD 01, BL D, Sl 207.<br />
                                    Ed. Fusion Work & Live<br />
                                    CEP: 70.701-040<br />
                                    Brasília – DF - Brasil
                                </p>
                            </div>

                        </div>

                    </div>

                    <div class="footer-sign">
                        <p>© Copyright 2018 <strong>Precious Brazil</strong>. <?php _e('Criação by', 'ps'); ?> <a href="http://prietoeassociados.com.br/" title="Prieto & Associados" target="_blank">Prieto</a></span>
                    </div>

                </div>

            </div>


        </div>

    </div>

    <a class="backtop btn -white" href="#top" title="<?php _e('Voltar ao Topo', 'ps'); ?>" data-scroll><i class="icon-arrow-up"></i></i></a>

</footer>


<script  src="<?php bloginfo('template_url') ?>/assets/js/vendors/jquery.331.js"></script>
<script  src="<?php bloginfo('template_url') ?>/assets/js/vendors/scrollto.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/vendors/magnific.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/source/main.js"></script>

<?php

    global $js;

    $default_scripts = array();

    populate_array($js, $default_scripts, true);
    get_scripts();

    wp_footer();

?>

</body>
</html>
