<?php

	$css = array('css/404.css');

	get_header();


?>

<content>

	<section class="_404 default" id="404">

		<div class="container-404 container">

			<div class="holder-404">

				<div class="content-404">

					<!-- <div class="image">

						<figure class="image-holder">

							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404.png" alt="página 404">

						</figure>

					</div> -->

					<div class="content">

						<div class="content-title row">

							<h1 class="offset-md-1 col-md-11"><span>Erro!</span> Página não encontrada</h1>

						</div>


						<div class="content-holder row align-items-center">

							<div class="left offset-md-1 col-md-4">
								<p>Opa!</p>
							</div>

							<div class="right offset-md-1 col-md-5">
								<p>Pedimos desculpas, mas a página que você procura não existe nesse site.</p>
							</div>

						</div>

					</div>

				</div>

				<div class="search-404 row">

					<div class="search-holder offset-md-1 col-md-10">

						<!-- <span class="title">Quer fazer uma nova busca?</span> -->

						<form class="search" action="<?php echo home_url( '/' ); ?>">
							<input type="search" name="s" placeholder="Quer fazer uma nova busca?" />
							<button type="submit" class="submit"><i class="fas fa-search"></i></button>
						</form>

					</div>


					<div class="navigation offset-md-1 col-md-10">

						<a href="<?php bloginfo('url') ?>" title="Quer voltar para a Página Inicial?">Quer voltar para a Página Inicial?</a>

					</div>


				</div>

			</div>

		</div>

	</section>

	<?php get_footer(); ?>
