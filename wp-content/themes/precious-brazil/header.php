<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<title>
		<?php if ( is_category() ) {
			echo 'Categoria &quot;'; single_cat_title(); echo '&quot; | '; bloginfo( 'name' );
		} elseif ( is_tag() ) {
			echo 'Tag &quot;'; single_tag_title(); echo '&quot; | '; bloginfo( 'name' );
		} elseif ( is_archive() ) {
			wp_title(''); echo ' Arquivos | '; bloginfo( 'name' );
		} elseif ( is_search() ) {
			echo 'Resultado da Busca &quot;'.wp_specialchars($s).'&quot; | '; bloginfo( 'name' );
		} elseif ( is_home() || is_front_page() ) {
			bloginfo( 'name' ); echo ' | '; bloginfo( 'description' );
		}  elseif ( is_404() ) {
			echo 'Página não encontrada | '; bloginfo( 'name' );
		} elseif ( is_single() ) {
			wp_title(''); echo ' | '; bloginfo( 'name' );
		} else {
			echo wp_title(''); echo ' | '; bloginfo( 'name' );
		} ?>
	</title>

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/images/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/images/favicons/favicon-16x16.png">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri() ?>/assets/images/favicons/favicon.ico">
	<meta name="msapplication-TileColor" content="#4f5054">
	<meta name="theme-color" content="#4f5054">


	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/grid.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/icons.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/assets/css/main.css">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">


	<?php

		// Default css
		global $css;

		// Quando é necessário carregar o css fora, descomente este código
		$default_stylesheets = array();
		populate_array($css, $default_stylesheets);
		get_stylesheets();

		wp_enqueue_script('jquery');
		wp_head();

	?>


</head>

<body <?php body_class(); ?> data-url="<?php echo site_url(); ?>" id="top">

	<main role="main">

		<header class="header">

			<?php /* search engine
			<div class="search" data-js="search__header">


				<div class="search-container container">

					<div class="search-holder">

						<form class="search-form" action="<?php bloginfo('url') ?>" method="get">

							<input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Pesquisar..." />
							<input class="btn -search" type="submit" value="Pesquisar" />

							<?php // define em qual cpt pesquisar ?>
							<input type="hidden" value="post" name="post_type" id="post_type" />

						</form>

					</div>

				</div>

			</div>
			*/ ?>

			<div class="header-container container">

				<div class="header-holder row justify-content-between">

					<div class="logo">
						<a href="<?php echo site_url(); echo get_bloginfo('language') == 'pt-br' ? '' : '/en' ?>" title="Precious Brazil">
							<img src="<?php bloginfo('template_url') ?>/assets/images/logo-preciousbrazil.svg" alt="Precious Brazil">
						</a>
					</div>

					<div class="menu-holder">

						<?php

							wp_nav_menu( array(
							    'theme_location' => 'menu-principal',
								'container'      => ''
							) );

						?>

						<?php /* <a class="btn__search" title="Pesquisa" data-js="toggle__search"><i class="icon-search"></i></a> */ ?>

						<div class="menu-hamburger">

							<a class="hamburger-nav-trigger" data-js="toggle__respmenu">
							   <span class="hamburger-nav-icon"></span>
							   <svg x="0px" y="0px" width="48px" height="48px" viewBox="0 0 54 54">
							      <circle fill="transparent" stroke="#66788f" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
							   </svg>
							</a>

						</div>

					</div>

				</div>

			</div>


		</header>
