<?php

	$css = array('css/single-empresas.css');

	get_header();

?>

<content>

	<section class="empresa default">

		<div class="empresa-container container">

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<article id="empresa-<?php the_ID(); ?>">

					<div class="empresa-title main-title center">
						<h1 class="title -pages"><?php the_title(); ?> <span class="subtitle"><?php _e('EMPRESAS', 'ps'); ?></span></h1>
					</div>

					<div class="empresa-links row">

						<div class="intern col-md-7 right">

							<?php if( get_field('empresa_site') ) : ?>
								<div class="redes-block">
									<a href="<?php the_field('empresa_site');?>" target="_blank"><i class="icon-url"></i> <?php _e('Visitar site', 'ps'); ?></a>
								</div>
							<?php endif; ?>

							<?php if( get_field('empresa_email') ) : ?>
								<div class="redes-block">
									<a href="mailto:<?php the_field('empresa_email');?>"><i class="icon-email"></i> <?php _e('Enviar Email', 'ps'); ?></a>
								</div>
							<?php endif; ?>

						</div>

						<div class="extern col-md-5 left">

							<?php if( get_field('empresa_facebook') ) : ?>
								<a href="<?php the_field('empresa_facebook');?>" target="_blank"><i class="icon-facebook2"></i></a>
							<?php endif; ?>

							<?php if( get_field('empresa_instagram') ) : ?>
								<a href="<?php the_field('empresa_instagram');?>" target="_blank"><i class="icon-instagram2"></i></a>
							<?php endif; ?>

							<?php if( get_field('empresa_whatsapp') ) : ?>
								<a href="<?php the_field('empresa_whatsapp');?>" target="_blank"><i class="icon-whatsapp"></i></a>
							<?php endif; ?>

						</div>

					</div>

					<div class="empresa-dados">

						<div class="dados d-flex justify-content-center">
							<span class="item"><strong><?php _e('Segmento principal:', 'ps'); ?></strong> <?php the_field('empresa_segmento'); ?></span>
							<span class="item"><strong><?php _e('Telefone:', 'ps'); ?></strong> <?php the_field('empresa_telefone'); ?></span>
							<span class="item"><strong><?php _e('Localização:', 'ps'); ?></strong> <?php the_field('empresa_localizacao'); ?></span>
						</div>

						<div class="content">
							<span class="line"><strong><?php _e('Perfil:', 'ps'); ?></strong> <?php the_field('empresa_perfil'); ?></span>
							<span class="line"><strong><?php _e('Produtos:', 'ps'); ?></strong> <?php the_field('empresa_produtos'); ?></span>
						</div>

					</div>



					<?php if( get_field('empresa_destaque_1') || get_field('empresa_destaque_2') || get_field('empresa_destaque_3') ) : ?>

					<div class="empresas-destaques">

						<span class="line"><strong><?php _e('Destaques:', 'ps'); ?></strong></span>

						<div class="empresas-destaques-holder row">

							<a class="image col-md-4" href="<?php the_field('empresa_destaque_1'); ?>" data-js="open-image">
								<img src="<?php the_field('empresa_destaque_1'); ?>" alt="">
							</a>

							<a class="image col-md-4" href="<?php the_field('empresa_destaque_2'); ?>" data-js="open-image">
								<img src="<?php the_field('empresa_destaque_2'); ?>" alt="">
							</a>

							<a class="image col-md-4" href="<?php the_field('empresa_destaque_3'); ?>" data-js="open-image">
								<img src="<?php the_field('empresa_destaque_3'); ?>" alt="">
							</a>

						</div>

					</div>

					<?php endif; ?>


					<div class="empresas-nav">

						<?php
							the_post_navigation(
								array(
				            		'prev_text' => _e( 'ANT' ),
				            		'next_text' => _e( 'PROX' ),
        						)
							);
						?>

					</div>

				</article>

			<?php endwhile; endif;?>

		</div>

	</section>

</content>

<?php get_footer(); ?>
