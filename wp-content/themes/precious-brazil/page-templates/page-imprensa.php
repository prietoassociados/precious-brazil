<?php

	// Template name: Imprensa

    $css = array('css/imprensa.css');

    get_header();


	$imp_empresa = $_POST['imp_empresa'];
	$imp_categoria = $_POST['imp_categoria'];

?>

<content>

	<section class="imprensa default">

		<div class="imprensa-container container">

			<div class="imprensa-title main-title center">

	            <h1 class="title -pages"><?php _e('IMPRENSA', 'ps') ?></h1>
				<h2 class="subtitle"><?php echo get_the_excerpt(); ?></h2>

	        </div>

			<div class="imprensa-content">

				<div class="imprensa-holder row" id="filter">

					<?php get_template_part( 'templates/template', 'filter-imprensa' ) ?>

				</div>

				<?php if( !$imp_empresa && !$imp_categoria ) : ?>
				<div class="imprensa-holder featured row">

					<div class="col-md-7">

						<div class="imprensa-holder-intern row">

							<?php

								$args = array(
							        'post_type'           => 'imprensa_post',
							        'posts_per_page'      => 5,
							        'offset' 			  => 1,
									'meta_query' => array(
										array(
											'key'     => 'imprensa_featured',
											'value'   => '1',
											'compare' => '==',
										)
									)
							    );

								$wp_query1 = new WP_Query($args);

							    if ( $wp_query1->have_posts() ) : while ( $wp_query1->have_posts() ) : $wp_query1->the_post();

							?>

				            <div class="col-md-6">

								<a class="imprensa-featured" href="<?php the_permalink();?>" title="<?php the_title(); ?>" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">

									<div class="featured-mask">

										<div class="icons">

											<?php

												$imprensa_type = get_field('imprensa_type');

												if (in_array('text', $imprensa_type)) echo '<i class="icon-doc"></i>';
												if (in_array('image', $imprensa_type)) echo '<i class="icon-midia"></i>';
												if (in_array('video', $imprensa_type)) echo '<i class="icon-youtube"></i>';

											?>

										</div>

									</div>

				            	</a>

				            </div>

							<?php endwhile; endif; wp_reset_postdata(); ?>

						</div>

					</div>

					<div class="col-md-5">

						<div class="imprensa-holder-intern row">

							<?php

								$args = array(
							        'post_type'           => 'imprensa_post',
							        'posts_per_page'      => 1,
									'meta_query' => array(
										array(
											'key'     => 'imprensa_featured',
											'value'   => '1',
											'compare' => '==',
										)
									)
							    );

								$wp_query2 = new WP_Query($args);

							    if ( $wp_query2->have_posts() ) : while ( $wp_query2->have_posts() ) : $wp_query2->the_post();

							?>

							<div class="imprensa-featured-holder col-md-12">

					            <a class="imprensa-featured -large" href="<?php the_permalink()?>" style="background: url('<?php echo get_the_post_thumbnail_url(); ?>')">

				            		<div class="featured-mask">

										<div class="icons">

											<?php

												$imprensa_type = get_field('imprensa_type');

												if (in_array('text', $imprensa_type)) echo '<i class="icon-doc"></i>';
												if (in_array('image', $imprensa_type)) echo '<i class="icon-midia"></i>';
												if (in_array('video', $imprensa_type)) echo '<i class="icon-youtube"></i>';

											?>

										</div>

									</div>

				            	</a>

							</div>

			       			 <?php endwhile; endif; wp_reset_postdata(); ?>

						</div>

					</div>

				</div>
				<?php endif; ?>

				<div class="imprensa-holder basic row">

					<div class="col-lg-9 imprensa-parts-content">

						<div class="imprensa-artigos">

							<?php

								$args = array(
									'post_type' 	 => 'imprensa_post',
									'order' 		 => 'DESC',
									'posts_per_page' => 5,
									'paged'          => get_query_var('paged') ? get_query_var('paged') : 1,
								);

								// No filters
								if( !isset($_POST['imp_empresa']) && !$_POST['imp_empresa'] ) {
									$args['meta_query'] = array(
										'key'     => 'imprensa_featured',
										'value'   => '0',
										'compare' => '==',
									);
								}


								// filter Empresa
								if( isset($_POST['imp_empresa']) && $_POST['imp_empresa'] ) {
									$args['tax_query'] = array(
										'operator' => 'AND',
										array(
											'taxonomy' => 'empresa_imprensa',
											'field'    => 'term_id',
											'terms'    =>  $_POST['imp_empresa'],
										),
									);
								}

								// filter Categoria
								if( isset($_POST['imp_categoria']) && $_POST['imp_categoria'] ) {
									$args['tax_query'][] = array(
										'taxonomy' => 'categoria_imprensa',
										'field'    => 'term_id',
										'terms'    =>  $_POST['imp_categoria'],
									);
								}




								$wp_query = new WP_Query( $args );

								if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();

							?>

			        		<article class="artigos">

								<a class="artigo-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

								<div class="artigo-meta">

									<div class="midia">
										<?php the_field('imprensa_nome_da_midia'); ?>
									</div>

									<div class="category">

										<?php the_terms( $post->ID, 'categoria_imprensa', '', ', ' ); ?>

									</div>

									<div class="icons">

										<?php

											$imprensa_type = get_field('imprensa_type');

											if (in_array('text', $imprensa_type)) echo '<i class="icon-doc"></i>';
											if (in_array('image', $imprensa_type)) echo '<i class="icon-midia"></i>';
											if (in_array('video', $imprensa_type)) echo '<i class="icon-youtube"></i>';

										?>

									</div>

								</div>

			        		</article>

						<?php endwhile; else: ?>

							<div class="response">
								<p><?php _e('Não tem nenhum Artigo com esses termos de pesquisa.', 'ps'); ?></p>
							</div>

						<?php wp_reset_postdata(); endif; ?>

						</div>

						<?php get_template_part('templates/template', 'pagination'); ?>

					</div>

					<div class="col-lg-3 imprensa-parts-info">

						<div class="imprensa-info">

							<?php echo get_post_field('post_content', 168); ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
