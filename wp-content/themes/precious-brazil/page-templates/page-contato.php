<?php

	// Template name: Contato

    $css = array('css/contato.css');

    get_header();

?>

<content>

	<section class="contato default">

		<div class="contato-title main-title center">

            <h1 class="title -pages"><?php _e('CONTATO', 'ps') ?></h1>

        </div>

		<div class="contato-container container">

			<div class="contato-holder row">

				<div class="col-lg-9 -left">

					<div class="contato-form">

						<?php echo do_shortcode('[contact-form-7 id="171" title="Formulário de Contato"]'); ?>

						<div class="contato-social">
							<a class="link" href="https://www.facebook.com/ibgm.brasil" target="_blank"><i class="icon-facebook2"></i></a>
							<a class="link" href="https://www.instagram.com/ibgmbrasil" target="_blank"><i class="icon-instagram2"></i></a>
						</div>

					</div>

				</div>

				<div class="col-lg-3 -right">

					<aside class="contato-info">

						<?php
							while ( have_posts() ) : the_post();
								the_content();
							endwhile;
						?>

					</aside>

				</div>

			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
