<?php

    // Template name: Ações

    $css = array('css/acoes.css');
    $js  = array('js/source/acoes.js');

    get_header();

?>

<content>

    <?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <section class="acoes-about">

        <div class="acoes-about-container container">

            <div class="acoes-about-holder row">

                <div class="acoes-about-intro col-lg-6">

                    <div class="main-title">

                        <h2 class="title -underlined -down"><?php the_title(); ?></h2>

                    </div>

                    <div class="content">
                        <?php the_content(); ?>
                    </div>


                </div>


            </div>

        </div>

    </section>

    <section class="acoes default" id="acoes">

        <div class="acoes-container container">

            <?php

                $g = 1; if( have_rows('acoes') ) while ( have_rows('acoes') ) : the_row();

                    echo '<div class="acoes-grupo">';

                        echo '<h2 class="title">'.get_sub_field('acoes-grupo').'</h2>';

                        echo '<div class="acoes-holder row">';

                    $i = 1 ; while ( have_rows('acoes-itens') ) : the_row(); ?>

                        <div class="acoes-item col-md-3 col-lg-2">

                            <div class="acoes-item-holder" <?php if( get_sub_field('image') ) : ?>style="background-image: url('<?php echo get_sub_field('image'); ?>')"<?php endif; ?> data-js="acoes-toggle" data-index="<?php echo 'g'.$g. '-' .$i; ?>">

                                <h6 class="name"><?php echo get_sub_field('title'); ?></h6>

                            </div>

                        </div>

                    <?php $i++; endwhile; ?>

                    </div>

                    <div class="acoes-content row">

                        <?php $i = 1 ; while ( have_rows('acoes-itens') ) : the_row(); ?>

                            <div class="acoes-content-holder col-md-6" data-child="<?php echo 'g'.$g. '-' .$i; ?>">

                                <h3 class="title-interno"><?php echo get_sub_field('title-interno') ?></h3>
                                <div class="content">
                                    <?php echo get_sub_field('content') ?>
                                </div>

                            </div>

                        <?php $i++; endwhile; ?>

                    </div>

                </div>

            <?php $g++; endwhile; ?>


        </div>

    </section>

    <?php endwhile; endif;?>

</content>

<?php get_footer(); ?>
