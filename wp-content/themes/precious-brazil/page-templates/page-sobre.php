<?php

    // Template name: Quem somos

    $css = array('css/sobre.css');
    $js  = array('js/vendors/mcustom-scroll.js', 'js/source/sobre.js');

    get_header();

?>

<content>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <section class="sobre default" id="sobre">

        <div class="sobre-title main-title center">

            <h1 class="title -pages"><?php the_title(); ?></h1>

        </div>

        <div class="sobre-content">

            <div class="sobre-container container">

                <div class="sobre-holder row">

                    <div class="image" style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'full' ); ?>');"></div>

                    <div class="content col-lg-6 offset-lg-6">

                        <h1 class="title"><?php the_title(); ?></h1>

                        <div class="text" data-js="sobre">
                            <?php echo get_field('sobre-intro'); ?>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <?php if(get_the_content()) : ?>
        <div class="sobre-content-default">

            <div class="sobre-container container">

                <div class="sobre-holder row">

                    <div class="sobre-content-dinamyc col-12">

                        <?php the_content(); ?>

                    </div>

                </div>

            </div>

        </div>
        <?php endif; ?>

        <div class="sobre-actions">

            <div class="sobre-actions-container container">

                <div class="sobre-actions-holder row">

                    <?php
                        $btn1 = get_field('sobre-btn1');
                        $btn2 = get_field('sobre-btn2');
                    ?>

                    <?php if( $btn1['text'] ) : ?>
                    <div class="action-left col-md-5 offset-md-1">

                        <div class="btn1">

                            <p class="text"><?php echo $btn1['text']; ?></p>

                            <?php if( $btn1['link'] ) : ?>
                                <div class="btn-control left">
                                    <a
                                        class="btn -black"
                                        href="<?php echo $btn1['link']['url']; ?>"
                                        title="<?php echo $btn1['link']['title']; ?>"
                                        target="<?php echo $btn1['link']['target']; ?>"
                                    >
                                        <?php echo $btn1['link']['title']; ?>
                                    </a>
                                </div>
                            <?php endif; ?>

                        </div>

                    </div>
                    <?php endif; ?>

                    <div class="action-right <?php echo $btn1['text'] ? 'col-md-5' : 'col-md-12 center'; ?>">

                        <div class="btn2">

							<h2 class="title <?php echo $btn1['text'] ? '' : '-center'; ?>"><?php echo $btn2['text']; ?></h2>

                            <?php if( $btn2['link'] ) : ?>
                                <div class="btn-control <?php echo $btn1['text'] ? 'right' : 'center'; ?>">
                                    <a
                                        class="btn -black"
                                        href="<?php echo $btn2['link']['url']; ?>"
                                        title="<?php echo $btn2['link']['title']; ?>"
                                        target="<?php echo $btn2['link']['target']; ?>"
                                    >
                                        <?php echo $btn2['link']['title']; ?>
                                    </a>
                                </div>
                            <?php endif; ?>

						</div>

                    </div>

                </div>

            </div>

        </div>

    </section>

    <?php endwhile; endif; ?>

</content>

<?php get_footer(); ?>
