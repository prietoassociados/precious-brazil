<?php

    // Template name: Blog

    $css = array('css/blog.css');


    get_header();

?>

<content>

    <section class="blog default" id="blog">

        <div class="main-title center" rel="blog-title">

            <h1 class="title -pages"><?php _e('NOTÍCIAS', 'ps') ?></h1>

        </div>

        <div class="blog-container container">

            <div class="blog-holder row" id="blog">

                <?php

                    $args = array(
                        'post_type'      => 'post',
                        'posts_per_page' => '12',
                        'paged'          => get_query_var('paged') ? get_query_var('paged') : 1
                    );


                    $wp_query = new WP_Query( $args );

                    if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();

                ?>

                    <?php get_template_part('templates/template', 'post'); ?>

                <?php endwhile; endif; wp_reset_postdata(); ?>

            </div>

            <?php get_template_part('templates/template', 'pagination'); ?>

        </div>

    </section>


</content>

<?php get_footer(); ?>
