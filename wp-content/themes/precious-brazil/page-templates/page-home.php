<?php

	// Template name: Home

	$css = array('css/home.css', 'css/empresas.css');
	$js  = array('js/vendors/slick.js', 'js/vendors/mcustom-scroll.js', 'js/source/filter-letter.js', 'js/source/home.js');


	get_header();

	$banner = array(
		'banner_id'    => 'home',
		'banner_array' => get_field('banners'),
	);

?>

<content>

	<?php custom_template_part('templates/template', 'banner', $banner); ?>

	<section class="about">

		<div class="about-container container">

			<div class="about-holder row">

				<div class="about-intro col-lg-6">

					<div class="main-title">

						<h2 class="title -underlined -down"><?php _e('QUEM SOMOS', 'ps') ?></h2>

					</div>

					<div class="content">
						<?php the_field('home-quemsomos-desc'); ?>
					</div>


				</div>


			</div>

		</div>

	</section>

	<section class="stats">

		<div class="stats-container container">

			<div class="stats-holder row">

				<div class="stats-item col-md-4">
					<div class="stats-content">
						<h6 class="number"><?php the_field('home-stats-empresas') ?></h6>
						<label class="label"><?php _e('empresas', 'ps') ?></label>
					</div>
				</div>

				<div class="stats-item col-md-4">
					<div class="stats-content">
						<h6 class="number"><?php the_field('home-stats-destinos') ?></h6>
						<label class="label"><?php _e('destinos', 'ps') ?></label>
					</div>
				</div>

				<div class="stats-item col-md-4">
					<div class="stats-content">
						<h6 class="number"><?php the_field('home-stats-exportados') ?></h6>
						<label class="label"><?php _e('milhões U$<br /> exportados', 'ps') ?></label>
					</div>
				</div>

			</div>

		</div>

	</section>

	<section class="posts">

		<div class="posts-holder container">

			<div class="main-title center" role="posts">

				<h2 class="title -thin -huge"><?php _e('NOTÍCIAS', 'ps') ?></h2>

			</div>

			<div class="posts-content row">

				<?php

				$args = array(
					'post_type'      => 'post',
					'posts_per_page' => 4,
				);

				$query = new WP_Query($args);

				if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();

				?>

				<article class="post col-lg-3 col-md-6">


					<a class="image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

						<?php

						if (has_post_thumbnail()) {
							the_post_thumbnail('thumbnail');
						} else {
							echo '<img src="http://via.placeholder.com/260x195" />';
						}
						?>

					</a>

					<div class="caption">

						<div class="post-title">
							<h4 class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
						</div>

					</div>

				</article>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>

			<div class="posts-newsletter row">

				<div class="label col-md-7">
					<h4><?php _e('Receba nossa newsletter', 'ps') ?></h4>
				</div>

				<div class="cta col-md-5">
					<a class="btn -white" href="https://podio.com/webforms/18760793/1353045" title="Cadastre-se" target="_blank"><?php _e('Cadastre-se', 'ps') ?></a>
				</div>

			</div>

		</div>

	</section>

	<section class="empresas default">

		<div class="empresas-container container">

			<div class="empresas-holder row">

				<div class="empresas-list col-xl-8">

					<div class="empresas-nav">
						<button class="empresa-btn active" type="button" data-js="empresa_change" value="A">A</button>

						<?php
							foreach(range('B', 'Z') as $letter) {
						    	echo '<button class="empresa-btn" type="button" data-js="empresa_change" value="'.$letter.'">'.$letter.'</button>';
							}
						?>
					</div>

					<div class="empresas-search">

						<div class="search">
							<input class="input" type="search" placeholder="Buscar por nome...">
							<button class="submit" type="button" data-js="empresa_search"><i class="icon-search"></i></button>
						</div>

					</div>

					<div class="empresas-receive" data-js="empresa">

						<?php

							query_posts(
								array (
									'post_type'   => 'empresa',
									'orderby' 	  => 'title',
									'order' 	  => 'ASC',
									'starts_with' => 'A'
								)
							);

							if ( have_posts() ) : while ( have_posts() ) : the_post();

						?>

						<div class="empresa">

							<div class="empresa-title">

								<div class="title-holder">
									<h3 class="title">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										<span class="category">
											<?php the_field('empresa_segmento'); ?>
										</span>
									</h3>
								</div>

							</div>

							<div class="empresa-content">
								<p><?php the_field('empresa_perfil'); ?></p>
							</div>

							<div class="empresa-icons">
								<?php if( get_field('empresa_facebook') ) : ?><a href="<?php the_field('empresa_facebook'); ?>" target="_blank"><i class="icon-facebook2"></i></a><?php endif; ?>
								<?php if( get_field('empresa_instagram') ) : ?><a href="<?php the_field('empresa_instagram'); ?>" target="_blank"><i class="icon-instagram2"></i></a><?php endif; ?>
								<?php if( get_field('empresa_whatsapp') ) : ?><a href="<?php the_field('empresa_whatsapp'); ?>" target="_blank"><i class="icon-whatsapp"></i></a><?php endif; ?>
							</div>

						</div>

						<?php endwhile; endif; wp_reset_query(); ?>

					</div>

				</div>

				<div class="empresas-title col-xl-4 d-flex align-items-center">

					<div class="title-holder">

						<div class="title-absolute">

							<h2 class="title title-home"><?php _e('Empresas <span>do<br /> precious</span> brazil', 'ps') ?></h2>

							<div class="btn-control right">
								<a class="btn -black" href="<?php bloginfo('url') ?>/empresas" title="Ver todas as Empresas"><?php _e('ver todas', 'ps') ?></a>
							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

	<section class="featured">

		<div class="presentation">

			<div class="presentation-container container">

				<div class="main-title">
					<h6 class="title -no-uppercase center"><?php the_field('home-presentation') ?></h6>
				</div>

				<div class="cta row">

					<a class="featured-btn col-md-6 right" href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/btn-home-featured-br.png" alt=""></a>
					<a class="featured-btn col-md-6 left" href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/btn-home-featured-eng.png" alt=""></a>

				</div>

			</div>

		</div>

		<div class="participate">

			<div class="participate-container container">

				<div class="main-title center">

					<h3 class="title"><?php _e('PARTICIPE DO PRECIOUS BRAZIL', 'ps') ?></h3>

				</div>

				<p class="content center"><?php _e('Se você já é exportador ou pretende iniciar o processo de exportação de gemas, joias,<br /> bijuterias, folheados ou artefatos de pedras, acesse o formulário de participação e<br /> ingresse no Precious Brazil', 'ps') ?></p>

				<div class="btn-control -center">
					<a class="btn -black" href="#" title="Acesse"><?php _e('ACESSE', 'ps') ?></a>
				</div>

			</div>

		</div>

	</section>

	<section class="instagram">

		<div class="instagram-container container">

			<div class="instagram-holder row">

				<?php

					$access_token = '5511877409.1677ed0.e1106e0bf4d943bfa58bbf3da53c40d5';
					$user_id 	  = '5511877409'; // https://codeofaninja.com/tools/find-instagram-user-id
					$limit 	      = '4';

					$feed = '';

					$response = wp_instagram_connect( "https://api.instagram.com/v1/users/" . $user_id . "/media/recent?access_token=" . $access_token . "&count=" . $limit );

					// print_r($response);

					if( $response ) {

						$feed = $response->data;

						foreach ($feed as $post) {
							echo '
							<div class="instagram-item col-md-3">
								<figure class="image">
									<a href="' . $post->link . '" title="' . $post->caption->text . '" target="_blank">
										<img src="' . $post->images->low_resolution->url . '" />
									</a>
									<figcaption class="caption">' . limit_chars($post->caption->text, 200) . '</figcaption>
								</figure>
							</div>';
						}
					}

				?>

			</div>

			<div class="btn-control -center">
				<a class="btn -black" href="https://www.instagram.com/precious.brazil/" title="Ver Instagram" target="_blank"><?php _e('VER INSTAGRAM', 'ps') ?></a>
			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
