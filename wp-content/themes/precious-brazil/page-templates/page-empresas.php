<?php

	// Template name: Empresas

	$css = array('css/empresas.css');
	$js  = array('js/source/filter-letter.js');


	get_header();

?>

<content>

	<section class="empresas default">

		<div class="empresas-container container">

			<div class="empresas-holder row">

				<div class="empresas-list -page col-md-12">

					<div class="main-title center">

						<h1 class="title -pages"><?php _e('Empresas', 'ps') ?></h1>

					</div>

					<div class="empresas-destaque">

						<?php

		                    $args = array(
		                        'post_type'      => 'empresa',
		                        'posts_per_page' => '1',
		                        'meta_query' => array(
		                    		array(
		                    			'key'     => 'empresa_destaque',
		                    			'value'   => '1',
		                                'compare' => '==',
		                    		)
		                    	)
		                    );

		                    $query = new WP_Query( $args );

		                    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

		                ?>

						<div class="destaque row">

							<div class="image col-xl-8">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php

									if (has_post_thumbnail()) {
										the_post_thumbnail('full');
									} else {
										echo '<img src="http://via.placeholder.com/800x420" />';
									}

								?>
								</a>
							</div>

							<div class="content col-xl-4">

								<div class="main-title">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<h1 class="title -thin"><?php the_title(); ?></h1>
									</a>
									<span class="segmento"><?php the_field('empresa_segmento'); ?></span>
								</div>

								<div class="description">
									<p class="text"><?php the_field('empresa_perfil'); ?></p>
								</div>

							</div>

						</div>


						<?php endwhile; endif; $do_not_duplicate[] = get_the_ID(); wp_reset_query(); ?>

					</div>

					<div class="empresas-navigation row align-items-center">

						<div class="empresas-nav -page col-lg-8">
							<?php
								foreach(range('A', 'Z') as $letter) {
							    	echo '<button class="empresa-btn" type="button" data-js="empresa_change" value="'.$letter.'">'.$letter.'</button>';
								}
							?>
						</div>

						<div class="empresas-search -page col-md-12 col-lg-4">

							<div class="search">
								<input class="input" type="search" placeholder="<?php _e('Quer buscar por uma empresa?', 'ps') ?>">
								<button class="submit" type="button" data-js="empresa_search"><i class="icon-search"></i></button>
							</div>

						</div>

					</div>

					<div class="empresas-receive row" data-js="empresa">

						<?php

							$args = array(
								'post_type'      => 'empresa',
								'posts_per_page' => '6',
								'orderby' 	  	 => 'title',
								'order' 	  	 => 'ASC',
								'post__not_in'   => $do_not_duplicate,
								'paged'          => get_query_var('paged') ? get_query_var('paged') : 1
							);

							$wp_query = new WP_Query( $args );

							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();

						?>

						<div class="empresa -page col-md-6 col-lg-4">

							<div class="empresa-title">

								<div class="title-holder">
									<h3 class="title">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										<span class="category">
											<?php the_field('empresa_segmento'); ?>
										</span>
									</h3>
								</div>

							</div>

							<div class="empresa-content">
								<p><?php the_field('empresa_perfil'); ?></p>
							</div>

							<div class="empresa-icons">
								<?php if( get_field('empresa_facebook') ) : ?><a href="<?php the_field('empresa_facebook'); ?>" target="_blank"><i class="icon-facebook2"></i></a><?php endif; ?>
								<?php if( get_field('empresa_instagram') ) : ?><a href="<?php the_field('empresa_instagram'); ?>" target="_blank"><i class="icon-instagram2"></i></a><?php endif; ?>
								<?php if( get_field('empresa_whatsapp') ) : ?><a href="<?php the_field('empresa_whatsapp'); ?>" target="_blank"><i class="icon-whatsapp"></i></a><?php endif; ?>
							</div>

						</div>

						<?php endwhile; endif; wp_reset_postdata(); ?>

					</div>

					<?php get_template_part('templates/template', 'pagination'); ?>

				</div>

			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
