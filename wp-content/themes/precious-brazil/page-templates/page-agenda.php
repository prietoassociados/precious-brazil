<?php

	// Template name: Agenda

	$css = array('css/agenda.css');
	$js  = array('js/vendors/moment.js', 'js/vendors/moment.locales.js', 'js/source/filter-date.js', 'js/source/tab.js');


	get_header();

?>

<content>

	<section class="agenda default">

		<div class="agenda-container container">

			<div class="agenda-holder row">

				<div class="agenda-list col-xl-10 offset-xl-1">

					<div class="agenda-title main-title center">

						<h1 class="title -pages"><?php the_title(); ?></h1>

					</div>

					<div class="agenda-navigation d-flex justify-content-end align-items-center">

						<div class="agenda-nav">

							<div class="form-control">

								<div class="select-control" id="month">

									<select data-js="agenda_change" data-month>

										<option value="Todos" selected="selected"><?php _e('Todos', 'ps') ?></option>

										<?php

											$today = getdate();

											$months = array(
												'1' => 'Janeiro',
												'2' => 'Fevereiro',
												'3' => 'Março',
												'4' => 'Abril',
												'5' => 'Maio',
												'6' => 'Junho',
												'7' => 'Julho',
												'8' => 'Agosto',
												'9' => 'Setembro',
												'10' => 'Outubro',
												'11' => 'Novembro',
												'12' => 'Dezembro'
											);

											foreach($months as $key => $month) : ?>

												<!-- <option value="<?php echo $key; ?>" <?php echo ($today['mon'] === $key) ? 'selected="selected"' : ''; ?>><?php echo $month; ?></option> -->
												<option value="<?php echo $key; ?>"><?php echo $month; ?></option>

											<?php endforeach; ?>

									</select>

								</div>

							</div>

							<div class="form-control">

								<div class="select-control" id="year">

									<select data-js="agenda_change" data-year>

										<option value="Todos" selected="selected"><?php _e('Todos', 'ps') ?></option>

										<?php

											$years = array();

											$args = array(
												'post_type'   => 'evento',
												'post_status' => array('publish', 'future'),
												'orderby' 	  => 'date',
												'order' 	  => 'DESC',
											);

											$wp_query = new WP_Query( $args );

											while ( $wp_query->have_posts() ) : $wp_query->the_post();

												$post_year = get_the_date('Y');
												if(!in_array($post_year, $years)) $years[] = $post_year;

											endwhile;

										?>

										<?php foreach ($years as $year): ?>

											<!-- <option value="<?php echo $year; ?>" <?php echo ($today['year'] == $year) ? 'selected="selected"' : ''; ?>><?php echo $year; ?></option> -->
											<option value="<?php echo $year; ?>"><?php echo $year; ?></option>

										<?php endforeach; ?>


									</select>

								</div>

							</div>

						</div>


					</div>

					<div class="agenda-receive" data-js="evento">

						<?php

							$args = array(
								'post_type'   	 => 'evento',
								'post_status' 	 => array('publish', 'future'),
								'posts_per_page' => '-1',
								'orderby' 	  	 => 'date',
								'order' 	  	 => 'ASC',
								//'paged'       => get_query_var('paged') ? get_query_var('paged') : 1,
								'date_query'  => array(
									'after' => array(
							            'year'  => $today['year'],
										'month' => $today['mon'],
										'day'   => $today['mday'],
									),
									'inclusive' => true,
							    ),

							);

							$wp_query = new WP_Query( $args );

							if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post();

						?>

						<div class="evento evento-<?php the_ID() ?> accordion">

							<input class="accordion-toggle" type="checkbox" checked>
							<i class="accordion-toggle-icon icon-more"></i>

							<div class="evento-header row">

								<div class="evento-logo col-md-2">
									<?php
								        if (has_post_thumbnail()) {
								            the_post_thumbnail('full');
								        } else {
								            echo '<img src="http://via.placeholder.com/150x150" />';
								        }
							        ?>
								</div>

								<div class="evento-title col-md-10">

									<div class="title-holder">

										<h2 class="title">
											<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										</h2>

										<time class="time"><?php echo get_field('evento_data'); ?></time>

									</div>

								</div>

							</div>

							<div class="content">

								<div class="evento-nav menu-tab">

									<nav class="tab-link-list" data-postid="<?php the_ID() ?>">
										<a title="Informações" data-id="tab-<?php the_ID() ?>-1" id="tab-<?php the_ID() ?>-1" class="tab-list tab-list-<?php the_ID() ?> active">Informações</a>
										<a title="Localização" data-id="tab-<?php the_ID() ?>-2" id="tab-<?php the_ID() ?>-2" class="tab-list tab-list-<?php the_ID() ?>">Localização</a>
										<?php /* <a title="Programa" data-id="tab-<?php the_ID() ?>-3" id="tab-<?php the_ID() ?>-3" class="tab-list tab-list-<?php the_ID() ?>">Programa</a> */ ?>
			                        </nav>

			                    </div>

								<div class="evento-content">

									<div class="tab-content tab-content-<?php the_ID() ?> active" data-id="tab-<?php the_ID() ?>-1" id="tab-<?php the_ID() ?>-1">

										<div class="evento-content-text">
											<?php echo get_field('evento_info'); ?>
										</div>

										<?php if( get_field('evento_images') ) : ?>
										<div class="evento-gallery">

											<div class="galeria-list row">

						                        <?php

													$galeria = get_field('evento_images');

						                            foreach( $galeria as $pic) : ?>

						                                <a class="image col-lg-3 col-md-6" data-js="open-image" href="<?php echo $pic['url']; ?>">

						                                    <img src="<?php echo $pic['sizes']['thumbnail']; ?>" alt="<?php echo $pic['alt']; ?>" />

						                                </a>

						                        <?php endforeach; ?>

						                    </div>

										</div>
										<?php endif; ?>

									</div>

									<div class="tab-content tab-content-<?php the_ID() ?>" data-id="tab-<?php the_ID() ?>-2" id="tab-<?php the_ID() ?>-2">

										<div class="evento-content-text">
											<?php echo get_field('evento_local'); ?>
										</div>

									</div>

									<div class="tab-content tab-content-<?php the_ID() ?>" data-id="tab-<?php the_ID() ?>-3" id="tab-<?php the_ID() ?>-3">

										<div class="evento-content-text">
											<?php echo get_field('evento_programa'); ?>
										</div>

									</div>

								</div>

								<div class="evento-footer row">

									<div class="evento-links col-md-6">
										<?php if( get_field('evento_facebook') ) : ?><a href="<?php echo get_field('evento_facebook'); ?>" target="_blank" title="Visitar evento no Facebook"><i class="icon-facebook2"></i></a><?php endif; ?>
										<?php if( get_field('evento_insta') ) : ?><a href="<?php echo get_field('evento_insta'); ?>" target="_blank" title="Visitar evento no Facebook"><i class="icon-instagram2"></i></a><?php endif; ?>
									</div>

									<div class="evento-time col-md-6 align-self-center right">

										<time class="time"><i class="icon-date"></i> <span data-time-value="<?php echo get_the_time('U'); ?>"></span></time>

									</div>

								</div>

							</div>

						</div>

						<?php endwhile; endif; wp_reset_postdata(); ?>

					</div>

					<?php // get_template_part('templates/template', 'pagination'); ?>

				</div>

			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
