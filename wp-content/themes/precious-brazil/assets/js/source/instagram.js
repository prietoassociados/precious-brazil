(function($) {

	console.log('instagram');

	let token  = '50022967.ca5aa21.3fa77f8e64284e98a8f212c2affa9daf',
    	userid = 50022967,
    	limit  = 4;

	$.ajax({
		url: 'https://api.instagram.com/v1/users/' + userid + '/media/recent', // or /users/self/media/recent for Sandbox
		dataType: 'jsonp',
		type: 'GET',
		data: {access_token: token, count: limit},
		success: function(data) {
			console.log(data);
			for( x in data.data ){
				$('[data-js=instagram]').append('<div class="image"><img src="'+data.data[x].images.low_resolution.url+'"></li>'); // data.data[x].images.low_resolution.url - URL of image, 306х306
				// data.data[x].images.thumbnail.url - URL of image 150х150
				// data.data[x].images.standard_resolution.url - URL of image 612х612
				// data.data[x].link - Instagram post URL
			}
		},
		error: function(data){
			console.log(data); // send the error notifications to console
		}
	});

})( jQuery );
