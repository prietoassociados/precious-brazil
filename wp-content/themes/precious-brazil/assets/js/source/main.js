(function($) {


    // header minify on scroll
    var scrollpos     = window.scrollY;
    var header        = document.querySelector('header');
    var header_height = header.offsetHeight;

    function add_class_on_scroll() {
        header.classList.add('-minify');
    }
    function remove_class_on_scroll() {
        header.classList.remove('-minify');
    }

    window.addEventListener('scroll', function() {
        scrollpos = window.scrollY;
        if (scrollpos >= header_height) {
            add_class_on_scroll();
        } else {
            remove_class_on_scroll();
        }
    });


    // scroll to
    var scroll = new SmoothScroll('a[href*="#"]', {
        // History
	    updateURL      : true, // Update the URL on scroll
        popstate       : true, // Animate scrolling with the forward/backward browser buttons (requires updateURL to be true)
        topOnEmptyHash : true
    });


    // hide menus e search modals
    $(window).click(function() {
        if($('.menu-hamburger, .menu').hasClass('open')) {
            toggleMenu();
        }
    });

	// menu hamburger
	$('[data-js="toggle__respmenu"]').click(function(e){
        e.stopPropagation();
        toggleMenu();
	});

    function toggleMenu() {
        $('html, body').toggleClass('noscroll');
        $('.menu-hamburger, .menu').toggleClass('open');
    }

    // search header
    // $('[data-js="toggle__search"]').click(function(){
    //     $(this).toggleClass('active');
    //     $('[data-js="search__header"]').toggleClass('open');
    // });


    // modal popup
	$('body').on('click', '[data-js=open-image]', function(e) {
		e.preventDefault();

		$(this).magnificPopup({
			type         : 'image',
			cursor       : null,
			removalDelay : 300,
			mainClass    : 'mfp-fade',
			verticalFit  : true,
			tLoading     : 'Carregando...',
			gallery      : {
			  enabled : true
			}
		}).magnificPopup('open');
	});


})( jQuery );
