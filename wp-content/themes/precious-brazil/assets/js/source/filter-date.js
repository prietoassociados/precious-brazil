(function($) {

    let container = $('[data-js=evento]'),
		datefrom  = $('[data-time-value]'),
        change    = $('[data-js=agenda_change]'),
        siteurl   = $('[data-url]').data('url');


	// change php dates to ago
	datefrom.each(function() {
	    let el = $(this),
			today = moment().format('X'),
	        postDate = el.attr('data-time-value'),
	        strTimeAgo = moment.unix(postDate).locale('pt-br').fromNow(),
			dateResult = (today < postDate) ? `Começará ` +strTimeAgo : `Começou ` +strTimeAgo;

		el.text(dateResult);
	});

    // bind date
	change.change(function() {
	    create_loader('init');
	    get_eventos(get_dates());
	});


	function get_dates() {
		let y = $('[data-year] option:selected').val(),
			m = $('[data-month] option:selected').val();

		return { year: y, month: m };
	}

    // submit ajax request
    function get_eventos(evento_date) {

        $.ajax({
            url: siteurl + '/wp-json/ps/v2/eventos',
            data: {
				'evento_year' : evento_date.year,
				'evento_month' : evento_date.month,
            },
            type: 'post',
            dataType: 'json',
            async: true,

            success: function(data) {
                create_loader('out');
                remove_pagination();
                data ? create_eventos(data) : create_error(evento_date);
            },
            error: function() {
                create_loader('out');
                create_error();
            }
        });
    }

    function create_eventos(data) {
        container.empty();
        data.map(evento => {
            container.append(create_template(evento));
        });
    }

	function create_gallery(image) {
		const image_template = `
			<a class="image col-lg-3 col-md-4" data-js="open-image" href="${image.url}">
				<img src="${image.sizes.thumbnail}" />
			</a>`;
		return image_template;
	}

    function create_template(evento) {

		let today = moment().format('X'),
			postDate = moment(evento.datefrom).format('x'),
			dateResult = (today < postDate) ? true : false;

        const template = `
            <div class="evento evento-${evento.id} accordion">

				<input class="accordion-toggle" type="checkbox" checked>
				<i class="accordion-toggle-icon icon-more"></i>


				<div class="evento-header row">

					<div class="evento-logo col-md-2">
						<img src="${evento.logo}" />
					</div>

					<div class="evento-title col-md-10">

						<div class="title-holder">

							<h3 class="title">
								<a href="${evento.link}" title="${evento.title}">${evento.title}</a>
							</h3>

							<time class="time">${evento.date}</time>

						</div>

					</div>

				</div>

				<div class="content">

					<div class="evento-nav menu-tab">
						<nav class="tab-link-list">
							${evento.info ? `<a title="Informações" data-id="tab-1" id="tab-1" class="tab-list active">Informações</a>` : ''}
							${evento.local ? `<a title="Localização" data-id="tab-2" id="tab-2" class="tab-list">Localização</a>` : ''}
							${evento.programa ? `<a title="Programa" data-id="tab-3" id="tab-3" class="tab-list">Programa</a>` : ''}
						</nav>
					</div>

	                <div class="evento-content">
						${evento.info ? `
							<div class="tab-content active" data-id="tab-1" id="tab-1">

								<div class="evento-content-text">${evento.info}</div>

                                ${evento.images ? `
								<div class="evento-gallery">
									<div class="galeria-list row">
										${evento.images.map((image) => {
											return `
												<a class="image col-lg-3 col-md-4" data-js="open-image" href="${image.url}">
													<img src="${image.sizes.thumbnail}" />
												</a>`;
								        	}).join('')
										}
									</div>
								</div>
                                ` : ''}

							</div>` : ""}

						${evento.local ? `<div class="tab-content" data-id="tab-2" id="tab-2"><div class="evento-content-text">${evento.local}</div></div>` : ''}
						${evento.programa ? `<div class="tab-content" data-id="tab-3" id="tab-3"><div class="evento-content-text">${evento.programa}</div></div>` : ''}
					</div>

					<div class="evento-footer row">

						<div class="evento-links col-md-6">
							${evento.facebook ? `<a href="${evento.facebook}" target="_blank"><i class="icon-facebook2"></i></a>` : ''}
		                    ${evento.instagram ? `<a href="${evento.instagram}" target="_blank"><i class="icon-instagram2"></i></a>` : ''}
						</div>

						<div class="evento-time col-md-6 align-self-center right">
							<time class="time"><i class="icon-date"></i> ${dateResult ? `Começará` : `Começou`} ${moment.unix(evento.datefrom).locale('pt-br').fromNow()}</time>
						</div>

					</div>

				</div>


            </div>`;

        return template;
    }

    function create_error(date) {
        container.empty();

        const template = `
            <div class="evento -error col-md-12">
                <p class="error">Nenhum evento encontrado em: '${date.month}/${date.year}'.</p>
            </div>`;
        container.append(template);
    }

    function create_loader(status) {
        container.empty();

        status === 'init' ?
            container.append(`
                <div class="evento -load center">
                    <span class="loader"><img src="${siteurl}/wp-content/themes/precious-brazil/assets/images/loader.svg" /></span>
                </div>`)
        : container.empty()
    }

    function remove_pagination() {
        $('#paginate').remove();
    }

})( jQuery );
