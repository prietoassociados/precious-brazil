(function($) {

    let container = $('[data-js=empresa]'),
        change    = $('[data-js=empresa_change]'),
        search    = $('[data-js=empresa_search]'),
        siteurl   = $('[data-url]').data('url');

    let body      = $('body'),
        inPage    = body.hasClass('page-template-page-empresas');



    // bind letter
    change.click(function() {
        const letter = $(this).val();
        create_loader('init');
        get_companys(letter);
        $(this).addClass('active').siblings().removeClass('active');
    });

    // bind search letters
    search.click(function() {
        const s = $(this).prev().val();
        create_loader('init');
        get_companys(s);
    });



    // submit ajax request
    function get_companys(letter) {

        $.ajax({
            url: siteurl + '/wp-json/ps/v2/empresas',
            data: {
                'letter' : letter
            },
            type: 'post',
            dataType: 'json',
            async: true,

            success: function(data) {
                create_loader('out');
                remove_pagination();
                data ? create_companys(data) : create_error(letter);
            },
            error: function(errorThrown) {
                create_loader('out');
                create_error();
            }
        });
    }

    function create_companys(data) {
        container.empty();
        data.map(empresa => {
            container.append(create_template(empresa));
        });
        container.mCustomScrollbar('destroy');
        container.mCustomScrollbar({theme: 'dark-3'});
    }

    function create_template(empresa) {
        const template = `
            <div class="empresa ${ inPage ? `col-md-4` : '' }" id="${empresa.id}">

                <div class="empresa-title">

                    <div class="title-holder">
                        <h3 class="title">
                            <a href="${empresa.link}" title="${empresa.title}">${empresa.title}</a>
                            <span class="category">${empresa.segmento}</span>
                        </h3>
                    </div>

                </div>

                <div class="empresa-content">
                    <p>${empresa.perfil}</p>
                </div>

                <div class="empresa-icons">
                    ${ empresa.facebook ? `<a href="${empresa.facebook}" target="_blank"><i class="icon-facebook2"></i></a>` : ''}
                    ${ empresa.instagram ? `<a href="${empresa.instagram}" target="_blank"><i class="icon-instagram2"></i></a>` : ''}
                    ${ empresa.whatsapp ? `<a href="${empresa.whatsapp}" target="_blank"><i class="icon-whatsapp"></i></a>` : ''}
                </div>

            </div>`;

        return template;
    }

    function create_error(letter) {
        container.empty();

        const template = `
            <div class="empresa -error col-md-12">
                <p class="error">Nenhuma empresa foi encontrada com: '${letter}'.</p>
            </div>`;
        container.append(template);
    }

    function create_loader(status) {
        container.empty();

        status === 'init' ?
            container.append(`
                <div class="empresa -load center">
                    <span class="loader"><img src="${siteurl}/wp-content/themes/precious-brazil/assets/images/loader.svg" /></span>
                </div>`)
        : container.empty()
    }

    function remove_pagination() {
        $('#paginate').remove();
    }

})( jQuery );
