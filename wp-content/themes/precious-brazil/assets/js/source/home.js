(function($) {

    // home slides
    let slide_container = $('[data-slick=home]');

	slide_container.slick({
        fade:          true,
        arrows:        true,
        infinite:      true,
        autoplay:      false,
        autoplaySpeed: 5000,
        prevArrow:     '<button type="button" class="slick-nav slick-prev"><i class="icon-arrow-left"></i></button>',
        nextArrow:     '<button type="button" class="slick-nav slick-next"><i class="icon-arrow-right"></i></button>',
    });


    // custom scroll
    let scroll_container = $('[data-js=empresa]');

    $(window).on('load', function() {
        scroll_container.mCustomScrollbar({
            theme: 'dark-3'
        });
    });


})( jQuery );
