(function($) {

    // custom scroll
    let scroll_container = $('[data-js=sobre]');

    $(window).on('load', function() {
        scroll_container.mCustomScrollbar({
            theme: 'light-3'
        });
    });


})( jQuery );
