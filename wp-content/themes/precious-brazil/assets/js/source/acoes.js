(function($) {

    // toggle acoes
    const index  = $('[data-index]');
    const childs = $('[data-child]');

    $('[data-js="acoes-toggle"]').unbind('click').bind('click', function (e) {

        let _self    = $(this),
            _content = $('[data-child='+_self.data('index')+']');

        acoesToggle(_self, _content);
    });

    function acoesToggle(self, content) {

        if(self.hasClass('active')){
			childs.slideUp(300);
			self.removeClass('active');
		}
		else{
			childs.slideUp(300);
			content.delay(300).slideDown(300);
			index.removeClass('active');
			self.addClass("active");
		}

    }


})( jQuery );
