$(document).ready(function () {

    // click function
    $('body').on('click', '.tab-list', function(e) {

        e.preventDefault();

        let tabId = $(this).data('id');
        let postId = $(this).parent().data('postid');

        console.log(postId);

        $('.tab-list-' +postId).removeClass('active');
        $('.tab-content-' +postId).removeClass('active');

        $('[data-id="'+tabId+'"]').addClass('active');

    });

});
