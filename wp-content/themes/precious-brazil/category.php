<?php

    $css = array('css/counter.css', 'css/blog.css');
    $js  = array('js/source/counter.js');


    get_header();

?>

<content>

    <?php get_template_part('templates/template', 'counter') ?>

    <section class="blog" id="blog">

        <div class="main-title -super row" rel="blog-title">

            <h1 class="title"><?php echo single_cat_title('', false); ?></h1>

        </div>

        <div class="blog-container container">

            <div class="blog-holder row" id="blog">

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <?php get_template_part('templates/template', 'post'); ?>

                <?php endwhile; endif; ?>

            </div>

            <?php get_template_part('templates/template', 'pagination'); ?>

        </div>

    </section>


</content>

<?php get_footer(); ?>
