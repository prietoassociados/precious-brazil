<?php

    $css = array('css/imprensa.css');

    get_header();

?>

<content>

	<section class="imprensa default">

		<div class="imprensa-container container">

			<div class="imprensa-title main-title center">

	            <h1 class="title -pages">IMPRENSA</h1>
				<h2 class="subtitle"><?php single_term_title(); ?></h2>

	        </div>

			<div class="imprensa-content">

				<div class="imprensa-holder basic row">

					<div class="col-md-9">

						<div class="imprensa-artigos">

							<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			        		<article class="artigos">

								<a class="artigo-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

								<div class="artigo-meta">

									<div class="midia">
										<?php the_field('imprensa_nome_da_midia'); ?>
									</div>

									<div class="category">

										<?php the_terms( $post->ID, 'categoria_imprensa', '', ', ' ); ?>

									</div>

									<div class="icons">

										<?php

											$imprensa_type = get_field('imprensa_type');

											if (in_array('text', $imprensa_type)) echo '<i class="icon-doc"></i>';
											if (in_array('image', $imprensa_type)) echo '<i class="icon-midia"></i>';
											if (in_array('video', $imprensa_type)) echo '<i class="icon-youtube"></i>';

										?>

									</div>

								</div>

			        		</article>

							<?php endwhile; endif; ?>

						</div>

						<?php get_template_part('templates/template', 'pagination'); ?>

					</div>

					<div class="col-md-3">

						<div class="imprensa-info -category">

							<h3>Empresas</h3>

							<nav class="imprensa-categories">

							<?php

								$args = array( 'hide_empty=0' );
								$terms = get_terms( 'empresa_imprensa', $args );

							    foreach ( $terms as $term ) {

									echo '<a href="' . esc_url( get_term_link( $term ) ) . '" title="' . esc_attr( sprintf( __( 'Todos os artigos de %s', 'precious-brazil' ), $term->name ) ) . '">' . $term->name . '</a>';

							    }

							?>

							</nav>

						</div>

						<div class="imprensa-info -category">

							<h3>Categorias</h3>

							<nav class="imprensa-categories">

							<?php

								$args = array( 'hide_empty=0' );
								$terms = get_terms( 'categoria_imprensa', $args );

							    foreach ( $terms as $term ) {

									echo '<a href="' . esc_url( get_term_link( $term ) ) . '" title="' . esc_attr( sprintf( __( 'Todos os artigos de %s', 'precious-brazil' ), $term->name ) ) . '">' . $term->name . '</a>';

							    }

							?>

							</nav>

						</div>

						<div class="imprensa-info">

							<?php echo get_post_field('post_content', 168); ?>

						</div>

					</div>

				</div>

			</div>

		</div>

	</section>

</content>

<?php get_footer(); ?>
