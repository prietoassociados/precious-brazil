<?php
/**
* Exemplos de como criar post types e taxonomias via código para não comprometer com plugin CPTUI.
*/

// Cria o post type
function custom_post_type_create() {

	// Empresas
	$labels = array(
        'name'                => 'Empresas',
        'singular_name'       => 'Empresa',
        'menu_name'           => 'Empresas',
        'parent_item_colon'   => 'Parent Empresa:',
        'all_items'           => 'Todas as Empresas',
        'view_item'           => 'Ver Empresa',
        'add_new_item'        => 'Adicionar nova Empresa',
        'add_new'             => 'Adicionar nova',
        'edit_item'           => 'Editar Empresa',
        'update_item'         => 'Atualizar Empresa',
        'search_items'        => 'Procurar Empresa',
        'not_found'           => 'Nenhuma Empresa encontrada',
        'not_found_in_trash'  => 'Nenhuma Empresa encontrada na lixeira',
    );
    $args = array(
        'label'               => 'empresa',
        'description'         => 'Lista de todas as Empresas',
        'labels'              => $labels,
        'supports'            => array('title', 'custom-fields', 'thumbnail'),
        'taxonomies'          => array(''),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-businessman',
        'can_export'          => true,
        'show_in_rest'        => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite' 			  => array( 'slug' => 'empresa', 'with_front' => true ),
        'capability_type'     => 'page',
    );

	register_post_type( 'empresa', $args );


	// Eventos
	$labels = array(
        'name'                => 'Eventos',
        'singular_name'       => 'Evento',
        'menu_name'           => 'Agenda',
        'parent_item_colon'   => 'Parent Evento:',
        'all_items'           => 'Todos os Eventos',
        'view_item'           => 'Ver Evento',
        'add_new_item'        => 'Adicionar novo Evento',
        'add_new'             => 'Adicionar novo',
        'edit_item'           => 'Editar Evento',
        'update_item'         => 'Atualizar Evento',
        'search_items'        => 'Procurar Evento',
        'not_found'           => 'Nenhum Evento encontrado',
        'not_found_in_trash'  => 'Nenhum Evento encontrado na lixeira',
    );
    $args = array(
        'label'               => 'evento',
        'description'         => 'Lista de todos os Eventos',
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'thumbnail'),
        'taxonomies'          => array(''),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-calendar',
        'can_export'          => true,
        'show_in_rest'        => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite' 			  => array( 'slug' => 'evento', 'with_front' => true ),
        'capability_type'     => 'page',
    );

	register_post_type( 'evento', $args );


	// Impresa
	$labels = array(
        'name'                => 'Artigos',
        'singular_name'       => 'Artigo',
        'menu_name'           => 'Imprensa',
        'parent_item_colon'   => 'Parent Artigo:',
        'all_items'           => 'Todos os Artigos',
        'view_item'           => 'Ver Artigo',
        'add_new_item'        => 'Adicionar novo Artigo',
        'add_new'             => 'Adicionar novo',
        'edit_item'           => 'Editar Artigo',
        'update_item'         => 'Atualizar Artigo',
        'search_items'        => 'Procurar Artigo',
        'not_found'           => 'Nenhum Artigo encontrado',
        'not_found_in_trash'  => 'Nenhum Artigo encontrado na lixeira',
    );
    $args = array(
        'label'               => 'imprensa_post',
        'description'         => 'Lista de todos os Artigos',
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'revisions', 'sticky', 'thumbnail'),
        'taxonomies'          => array(''),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-calendar',
        'can_export'          => true,
        'show_in_rest'        => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite' 			  => array( 'slug' => 'imprensa_post', 'with_front' => true ),
        'capability_type'     => 'page',
    );

	register_post_type( 'imprensa_post', $args );

}

add_action( 'init', 'custom_post_type_create' );


/* Cria as taxonomias */
function custom_taxonomy_create() {

	$cc_est_labels = array(
        'name'                       => 'Empresas',
        'singular_name'              => 'Empresa',
        'menu_name'                  => 'Empresas',
        'all_items'                  => 'Todas as empresas',
        'parent_item'                => 'Pertence a',
        'parent_item_colon'          => 'Pertence a:',
        'new_item_name'              => 'Renomear essa empresa',
        'add_new_item'               => 'Adicionar nova empresa',
        'edit_item'                  => 'Editar empresa',
        'update_item'                => 'Atualizar empresa',
        'separate_items_with_commas' => 'Separar empresas por vírgulas',
        'search_items'               => 'Procurar empresas',
        'add_or_remove_items'        => 'Adicionar ou remover empresas',
        'choose_from_most_used'      => 'Escolha entre as empresas mais utilizadas',
        'not_found'                  => 'Nenhuma empresa encontrada',
    );
    $cc_est_args = array(
        'labels'                     => $cc_est_labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );

	register_taxonomy( 'empresa_imprensa', array( 'imprensa_post' ), $cc_est_args );
	

	// Categoria de Imprensa
	$cc_est_labels = array(
        'name'                       => 'Categorias',
        'singular_name'              => 'Categoria',
        'menu_name'                  => 'Categorias',
        'all_items'                  => 'Todas as categorias',
        'parent_item'                => 'Pertence a',
        'parent_item_colon'          => 'Pertence a:',
        'new_item_name'              => 'Renomear essa categoria',
        'add_new_item'               => 'Adicionar nova categoria',
        'edit_item'                  => 'Editar categoria',
        'update_item'                => 'Atualizar categoria',
        'separate_items_with_commas' => 'Separar categorias por vírgulas',
        'search_items'               => 'Procurar categorias',
        'add_or_remove_items'        => 'Adicionar ou remover categorias',
        'choose_from_most_used'      => 'Escolha entre as categorias mais utilizadas',
        'not_found'                  => 'Nenhuma categoria encontrada',
    );
    $cc_est_args = array(
        'labels'                     => $cc_est_labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );

	register_taxonomy( 'categoria_imprensa', array( 'imprensa_post' ), $cc_est_args );

}

add_action( 'init', 'custom_taxonomy_create', 0 );
