<?php

// function to return filtered posts
// init get posts letter A
function wpps_posts_where( $where, $query ) {
    global $wpdb;

    $starts_with = $query->get( 'starts_with' );

    if ( $starts_with ) {
        $where .= " AND $wpdb->posts.post_title LIKE '$starts_with%'";
    }

    return $where;
}
add_filter( 'posts_where', 'wpps_posts_where', 10, 2 );



// set rest api for companys by letter
add_action( 'rest_api_init', function() {
    //Path to REST route and the callback function
    register_rest_route( 'ps/v2', '/empresas/', array(
        'methods'  => 'POST',
        'callback' => 'get_company_by_letter'
    ) );
});

function get_company_by_letter() {

    if ( isset($_POST) ) {

        $letter = $_POST['letter'];

        $args = array (
            'post_type'   => 'empresa',
            'lang'        => get_bloginfo('language') == 'pt-br' ? 'pt' : 'en',
            'orderby'     => 'title',
            'order'       => 'ASC',
            'post_status' => 'publish',
            'starts_with' => $letter,
        );

        $query = new WP_Query( $args );

        if( $query->have_posts() ) : while( $query->have_posts() ) : $query->the_post();

            $fields[] = array(
                'id'        => get_the_ID(),
                'title'     => get_the_title(),
                'link'      => get_the_permalink(),
                'segmento'  => get_field('empresa_segmento'),
                'perfil'    => get_field('empresa_perfil'),
                'facebook'  => get_field('empresa_facebook'),
                'whatsapp'  => get_field('empresa_whatsapp'),
                'instagram' => get_field('empresa_instagram'),
            );

        endwhile; endif;

        return $fields;
    }
}
