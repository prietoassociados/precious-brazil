<?php

require 'templateHelper/templateHelper.php';
function custom_template_part($slug, $name = null, $params = []) {
    JPR\TemplatePartWithVars\Helper::getTemplatePartWithNamedVariables($slug, $name, $params);
}

// categories no links
function user_the_categories() {
    global $cats;
    $cats = get_the_category();
    echo $cats[0]->cat_name;
    for ($i = 1; $i < count($cats); $i++) { echo ', ' . $cats[$i]->cat_name; }
}

 // $pid = The ID of the page we're looking for pages underneath
 // load details about this page
function is_tree($pid) {
	global $post;
	if( is_page() && ( $post->post_parent == $pid || is_page($pid) ) )
        return true;
	else
        return false;
};

// limit chars
function limit_chars($str, $limit) {
    $string = strip_tags($str);
    if (strlen($string) > $limit) {

        // truncate string
        $stringCut = substr($string, 0, $limit);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
    }
    return $string;
}

// instagram feed
function wp_instagram_connect( $api_url ){
	$connection_c = curl_init(); // initializing
	curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
	curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
	curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
	$json_return = curl_exec( $connection_c ); // connect and get json data
	curl_close( $connection_c ); // close connection
	return json_decode( $json_return ); // decode and return
}
