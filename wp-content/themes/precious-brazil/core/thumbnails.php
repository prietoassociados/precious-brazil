<?php
/**
 * Thumbnails
 */

/**
 * Default WordPress sizes
 */

update_option('thumbnail_size_w', 260);
update_option('thumbnail_size_h', 195);

update_option('medium_size_w', 350);
update_option('medium_size_h', 265);

update_option('large_size_w', 1024);
update_option('large_size_h', 768);

/**
 * Custom sizes
 */
add_action( 'after_setup_theme', 'theme_setup' );
function theme_setup() {

	// posts thumbs
	//add_image_size('posts', 586, 416, true );


}
