<?php
/**
* Loader
*/

/**
* Echoes scripts defined by the view
*/
function get_scripts() {
    global $js;
    if (isset($js)):
        foreach ($js as $script) { ?>

            <?php if(strpos($script, "https://") !== false || strpos($script, "http://") !== false) : ?>
                <script src="<?php echo $script; ?>"></script>

            <?php else : ?>
                <script src="<?php echo PATH_TEMPLATE ?>/assets/<?php echo $script; ?>"></script>

            <?php endif; ?>

            <?php
        }
    endif;
}

/**
* Echoes stylesheets defined by the view
*/
function get_stylesheets() {
    global $css;
    if (isset($css)):
        foreach ($css as $stylesheet) { ?>
            <link rel="stylesheet" href="<?php echo PATH_TEMPLATE ?>/assets/<?php echo $stylesheet; ?>" />
            <?php
        }
    endif;
}

/**
* Populates array with values from an array
*
* @param array $target Array to be populated
* @param array $values Values to populate $target
* @param bool $beginning Populate unshifting array
*/
function populate_array(&$target, $values, $beginning = false) {
    if ($beginning) {
        if (isset($target)) {
            foreach ($target as $tgt) {
                $values[] = $tgt;
            }
        }
        $target = $values;
    } else {
        foreach ($values as $value) {
            $target[] = $value;
        }
    }
}
