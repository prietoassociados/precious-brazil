<?php
/**
 * Init
 */

/**
 * Theme support
 */
add_theme_support('post-thumbnails');
add_theme_support('menus');

/**
 * Menus
 */
register_nav_menu('menu-principal', 'Menu Principal');

/**
 * Path constants
 */
define('PATH_TEMPLATE', get_bloginfo('template_directory'));
define('PATH_IMAGES',   get_bloginfo('template_directory') . '/assets/images');
define('PATH_URL',      get_bloginfo('url'));


/**
 * Google maps API key
 */
function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyD3Xn4ZeSyLycmq2w8U7tlISgtaBTp6lDE');
}

add_action('acf/init', 'my_acf_init');
