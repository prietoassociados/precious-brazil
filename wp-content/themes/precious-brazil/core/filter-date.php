<?php

// set rest api for schedules by date
add_action( 'rest_api_init', function() {
    //Path to REST route and the callback function
    register_rest_route( 'ps/v2', '/eventos/', array(
        'methods'  => 'POST',
        'callback' => 'get_schedules_by_date'
    ) );
});



function get_schedules_by_date() {

	$year  = $_POST["evento_year"];
	$month = $_POST["evento_month"];

    $args = array (
        'post_type'   => 'evento',
		'post_status' => array('publish', 'future'),
        'lang'        => get_bloginfo('language') == 'pt-br' ? 'pt' : 'en',
		'orderby' 	  => 'date',
		'order' 	  => 'ASC',

		'date_query'  => array(
			'year'  => $year,
			'month' => $month,
		),
    );

    $query = new WP_Query( $args );

    while( $query->have_posts() ) : $query->the_post();

        $fields[] = array(
            'id'        => get_the_ID(),
			'title'     => get_the_title(),
			'datefrom'  => get_the_date('U'),
            'link'      => get_the_permalink(),
			'logo'		=> (has_post_thumbnail() ? get_the_post_thumbnail_url(get_the_ID(), 'full') : 'http://via.placeholder.com/150x150'),
			'info'      => get_field('evento_info'),
            'local'     => get_field('evento_local'),
			'programa'  => get_field('evento_programa'),
			'images'    => get_field('evento_images'),
            'facebook'  => get_field('evento_facebook'),
            'instagram' => get_field('evento_insta'),
			'date'  	=> get_field('evento_data'),

        );

    endwhile;

    wp_send_json($fields);

}
