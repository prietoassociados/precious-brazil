<?php

// custom login
function logo_custom() { ?>

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">

	<style type="text/css">

		body.login {
			font-family: 'Montserrat', sans-serif;
			background-color: #65666a !important;
		}
		body.login div#login h1 a {
			background-image: url("<?php echo get_bloginfo('template_directory') ?>/assets/images/logo-preciousbrazil.svg");
			background-color: #65666a;
			background-position: center;
			background-size: 100%;
			width: 315px;
			height: 100px;
			margin: 0 0 20px;
			padding-bottom: 0;

			-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;
		}
		.login form {
			background-color: #65666a !important;
			box-shadow: none !important;
			margin-top: 0 !important;
			padding: 0 !important;
		}
		.login form .input {
			background: none !important;
		    border: none;
			border-radius: 6px;
		    padding: 15px 30px !important;
		    color: #4C4C4C;
		    font-size: 1.6em !important;
		    font-weight: 100;
		    margin-bottom: 20px !important;
		    min-width: 100%;
		    max-width: 100%;
		}
		.login label {
			color: #fff;
		}
		.wp-core-ui .button.button-large,
		.wp-core-ui .button-group.button-large .button {
		    border-color: none !important;
		    border-radius: 0 !important;
		    box-shadow: none !important;
		    text-shadow: none !important;
		    height: auto !important;
		    letter-spacing: 0;
		    transition: .3s ease-in-out;
		    background-color: transparent;
		    font-size: 1.2em;
		    font-weight: 700;
		    color: #222;
		    padding: 12px 50px !important;
		    border: 3px solid #222;
			border-radius: 4px;
		    text-decoration: none !important;
			text-transform: uppercase;
		    display: inline-block;
		    cursor: pointer;
		}
		.wp-core-ui .button.button-large:hover,
		.wp-core-ui .button-group.button-large .button:hover {
			background: #222 !important;
			border-color: #222;
			color: #fff !important;
		}
		.login #backtoblog a, .login #nav a {
		    color: #ccc !important;
		}
		.login #backtoblog a:hover,
		.login #nav a:hover,
		.login h1 a:hover {
			color: #fff !important;
		}
	</style>

<?php }

add_action('login_enqueue_scripts', 'logo_custom');
