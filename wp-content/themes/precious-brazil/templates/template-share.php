<!-- ========> Twitter script: <======== -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


<!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


<!-- ========> Google+ script: <========
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script> -->


<!-- ========> Pinterest script: <======== -->
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>

<!-- ========> LinkedIn script: <========
<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script> -->

<?php

	// Get current page URL
	$URL = urlencode(get_permalink());

	// Get current page title
    $Title = str_replace( ' ', '%20', get_the_title());
    // $Title = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');

	// Get Post Thumbnail for pinterest
	$Thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

	// Construct sharing URL without using any script
	$twitterURL   = 'https://twitter.com/intent/tweet?text='.$Title.'&amp;url='.$URL.'&amp';
	$facebookURL  = 'https://www.facebook.com/sharer.php?u='.$URL;
    $whatsappURL  = 'whatsapp://send?text='.$Title . ' ' . $URL;
    $pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$URL.'&amp;media='.$Thumbnail[0].'&amp;description='.$Title;

	// $googleURL   = 'https://plus.google.com/share?url='.$crunchifyURL;
	// $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$crunchifyURL.'&amp;title='.$crunchifyTitle;


    // Add sharing button at the end of page/page content
	$content .= '<div class="social">';
    $content .= '<a class="icon-1x facebook" href="'.$facebookURL.'" target="_blank"><i class="fab fa-facebook"></i></a>';
    $content .= '<a class="icon-1x twitter" href="'.$twitterURL.'" target="_blank"><i class="fab fa-twitter"></i></a>';
	$content .= '<a class="icon-1x whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="fab fa-whatsapp"></i></a>';
    $content .= '<a class="icon-1x pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><i class="fab fa-pinterest"></i></a>';

	// $content .= '<a class="icon-1x googleplus" href="'.$googleURL.'" target="_blank"><i class="fab fa-googleplus"></i></a>';
	// $content .= '<a class="icon-1x linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fab fa-linkedin"></i></a>';

	$content .= '</div>';

	echo $content;

?>
