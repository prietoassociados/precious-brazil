<section class="banner" id="banner-<?php echo $banner_id; ?>">

    <div class="banner-holder" data-slick="<?php echo $banner_id; ?>">


        <?php foreach($banner_array as $banner) : ?>

        <div class="banner-slide">

            <div class="banner-image">

                <div class="banner-image-bg" style="background-image: url(<?php echo $banner['image']; ?>);"></div>

                <!-- <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /> -->

            </div>

            <div class="banner-caption container">

                <h1 class="title"><?php echo $banner['title']; ?></h1>
                <h2 class="subtitle"><?php echo $banner['subtitle']; ?></h2>

                <div class="content">
                    <?php echo $banner['content'] ?>
                </div>

                <?php if($banner['link']) : ?>
                    <div class="banner-link">
                        <a class="btn -rounded -icon" href="<?php echo $banner['link']['url']; ?>" target="<?php echo $banner['link']['target']; ?>">
                            <?php _e('Saiba mais', 'ps'); ?>
                            <i class="left icon-arrow-right2"></i>
                        </a>
                    </div>
                <?php endif; ?>

            </div>

        </div>

        <?php endforeach; ?>


    </div>

</section>
