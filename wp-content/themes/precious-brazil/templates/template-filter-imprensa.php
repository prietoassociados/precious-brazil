
<div class="filter -imprensa col-md-12">

	<div class="form-control row">

		<div class="filter-filters col-lg-12">

			<form class="form-filter row align-items-end justify-content-end" method="post" action="<?php bloginfo('url') ?>/imprensa/">

				<div class="input-control col-lg-5 col-xl-5">

					<span class="title"><?php _e('Escolha uma Empresa', 'ps'); ?></span>

					<div class="select-control">

						<select id="empresa" name="imp_empresa">

							<option value="" <?php echo ($_POST['imp_empresa'] == '') ? ' selected="selected"' : ''; ?>><?php _e('Todos', 'ps'); ?></option>

							<?php
								$tax_terms = get_terms('empresa_imprensa', array('hide_empty' => '0', 'parent' => 0));
								foreach ( $tax_terms as $tax_term ):
									echo '<option value="'.$tax_term->term_id.'"';
									echo ($_POST['imp_empresa'] == ''.$tax_term->term_id.'') ? ' selected="selected"' : '';
									echo '>'.$tax_term->name.'</option>';
								endforeach;
							?>

						</select>

					</div>

				</div>

				<div class="input-control col-lg-5 col-xl-5">

					<span class="title"><?php _e('Escolha uma Categoria', 'ps'); ?></span>

					<div class="select-control">

						<select id="categoria" name="imp_categoria">

							<option value="" <?php echo ($_POST['imp_categoria'] == '') ? ' selected="selected"' : ''; ?>><?php _e('Todos', 'ps'); ?></option>

							<?php
								$tax_terms = get_terms('categoria_imprensa', array('hide_empty' => '0', 'parent' => 0));
								foreach ( $tax_terms as $tax_term ):
									echo '<option value="'.$tax_term->term_id.'"';
									echo ($_POST['imp_categoria'] == ''.$tax_term->term_id.'') ? ' selected="selected"' : '';
									echo '>'.$tax_term->name.'</option>';
								endforeach;
							?>

						</select>

					</div>

				</div>

				<div class="input-control col-lg-2 col-xl-2">
					<button class="btn -black" type="submit"><?php _e('Filtrar', 'ps'); ?></button>
				</div>

			</form>

		</div>

	</div>

</div>
