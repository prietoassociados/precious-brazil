<article class="post col-lg-4 col-md-6">

    <a class="image" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">

        <?php

	        if (has_post_thumbnail()) {
	            the_post_thumbnail('medium');
	        } else {
	            echo '<img src="http://via.placeholder.com/350x265" />';
	        }

        ?>

    </a>

    <div class="caption">

        <div class="post-title">

            <h4 class="title">
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?> - <span class="date"><?php the_date('d/m/Y'); ?></a></span>
            </h4>

        </div>

    </div>

</article>
