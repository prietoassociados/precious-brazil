<?php

    $css = array('css/counter.css', 'css/page.css');
    $js  = array('js/source/counter.js');

    get_header();

?>

<content>

    <?php get_template_part('templates/template', 'counter') ?>

    <section class="page default">

        <div class="page-container container">

            <div class="page-content row">

                <?php get_template_part('templates/template', 'page'); ?>

            </div>

            <?php // get_template_part('templates/template', 'page_cta'); ?>

        </div>

    </section>

</content>

<?php get_footer(); ?>
