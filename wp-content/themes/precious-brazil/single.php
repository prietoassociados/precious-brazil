<?php

    $css = array('css/single.css');


    get_header();

?>

<content>

    <section class="post-single" id="single">

        <div class="single-container container">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

            <div class="single-header row">

                <div class="title col-lg-9">

                    <div class="post__title">
                        <h1><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
                    </div>

                    <div class="post__tags">
                        <?php the_tags( '<ul class="tags"><li class="tag">', '</li><li class="tag">', '</li></ul>' ); ?>
                    </div>

                    <div class="post__socials">

                        <div class="social">

                            <a class="twitter" href="http://twitter.com/share?url=<?php echo get_the_permalink() ?>;text=<?php echo get_the_title() ?>" target="_blank">
                                <i class="icon-twitter"></i>
                            </a>

                            <a class="facebook" href="http://www.facebook.com/sharer.php?u=<?php echo get_the_permalink() ?>" target="_blank">
                                <i class="icon-facebook"></i>
                            </a>

                            <a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo get_the_permalink() ?>" target="_blank">
                                <i class="icon-linkedin"></i>
                            </a>

                        </div>

                        <div class="date">
                            <i class="icon-date"></i> <?php the_date('d.m.Y'); ?>
                        </div>

                    </div>

                </div>

            </div>

            <div class="post-content row">

                <article id="post-<?php the_ID(); ?>" <?php post_class('post col-lg-8'); ?>>

                    <div class="content">

                        <?php the_content(); ?>

                    </div>

                    <div class="post-relateds">

                        <div class="relateds-container">

                            <div class="relateds-holder">

                                <div class="relateds-title">

                                    <div class="title-container">

                                        <h3 class="title"><img src="<?php echo get_template_directory_uri() ?>/assets/images/icon-noticias-relacionadas.png" /> <?php _e('Notícias relacionadas', 'ps') ?></h3>

                                    </div>

                                </div>

                                <div class="relateds-posts row d-flex justify-content-between">

                                    <?php

                                        $category_array = get_the_category( get_the_ID() );
                                        $category_id    = $category_array[0]->term_id;

                                        $query = new WP_Query(
                                            array(
                                                'post_type'      => 'post',
                                                'category__in'   => array( $category_id ),
                                                'post__not_in'   => array( get_the_ID() ),
                                                'orderby'        => 'rand',
                                                'posts_per_page' => 3
                                            )
                                        );

                                        if ( $query->have_posts() ) while ( $query->have_posts() ) : $query->the_post();

                                    ?>


                                        <?php get_template_part('templates/template', 'post'); ?>


                                    <?php endwhile; wp_reset_postdata(); ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </article>


                <div class="sidebar col-lg-4">

                    <div class="sidebar-info -apex">
                        <h6><?php _e('Sobre a Apex-Brasil', 'ps') ?></h6>
                        <?php echo get_post_field('post_content', 94); ?>
            		</div>

                    <div class="sidebar-info -precious">
                        <h6><?php _e('Sobre o Precious Brazil', 'ps') ?></h6>
                        <?php echo get_post_field('post_content', 91); ?>
            		</div>

                </div>

            </div>

            <?php endwhile; endif; ?>

        </div>

    </section>

</content>

<?php get_footer();
