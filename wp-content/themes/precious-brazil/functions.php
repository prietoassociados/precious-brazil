<?php

require_once locate_template('core/custom.php');
require_once locate_template('core/init.php');
require_once locate_template('core/loader.php');
require_once locate_template('core/cleanup.php');
require_once locate_template('core/thumbnails.php');
require_once locate_template('core/func.php');
require_once locate_template('core/pagination.php');
require_once locate_template('core/post-types.php');

// Filter by first letter - companys
require_once locate_template('core/filter-letter.php');

// Filter by date - schedule
require_once locate_template('core/filter-date.php');
