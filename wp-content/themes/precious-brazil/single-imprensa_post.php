<?php

    $css = array('css/single-imprensa.css');

    get_header();

?>

<content>

	<section class="imprensa default">

		<div class="imprensa-container container">

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				<article id="imprensa-<?php the_ID(); ?>">

					<div class="imprensa-title main-title center">
						<h1 class="title -pages"><?php the_title(); ?> <span class="subtitle">IMPRENSA</span></h1>
					</div>

					<div class="imprensa-links">

						<div class="redes-block">
							<a href="<?php the_field('empresa_site');?>" target="_blank"><i class="icon-date"></i> <?php the_date('d.m.Y'); ?></a>
						</div>

						<?php if( get_field('imprensa_link_da_noticia') ) : ?>
							<div class="redes-block">
								<a href="<?php the_field('imprensa_link_da_noticia');?>"><i class="icon-ico-link"></i> Link da Notícia</a>
							</div>
						<?php endif; ?>

						<?php if( get_field('imprensa_nome_da_midia') ) : ?>
							<div class="redes-block">
								<i class="icon-news"></i> <?php the_field('imprensa_nome_da_midia'); ?>
							</div>
						<?php endif; ?>

					</div>

					<div class="imprensa-dados">
						<span class="item"><strong>Categoria: </strong><?php the_field('imprensa_categoria')?></span>
						<span class="item"><strong>Referência: </strong><?php the_field('imprensa_referencia')?></span>
						<span class="item"><strong>Marcas/Empresas participantes citadas: </strong><?php the_field('imprensa_participantes_citados')?></span>
					</div>

					<div class="imprensa-content">

						<?php the_content()?>

					</div>

				</div>

			    </article>

			<?php endwhile; endif;?>

		</section>

	</div>

</content>

<?php get_footer(); ?>
