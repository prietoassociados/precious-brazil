<?php

	$css = array('css/search.css');


	get_header();

	// vars da pesquisa
	$research = new WP_Query("s=$s&showposts=-1");
	$search   = esc_html($s, 1);
	$total    = $research->post_count;

?>

<content>

	<?php if( $total ) : ?>

    <section class="search default" id="search">

        <div class="search-container container">

            <div class="search-holder row">

				<div class="search-title main-title col-md-12">

					<h1 class="title -pages"><?php _e('RESULTADO DA BUSCA', 'ps'); ?></h1>
					<h2 class="subtitle"><?php _e('Você procurou por', 'ps'); ?></h2>
					<h6 class="key">"<?php echo $search; ?>"</h6>

					<?php wp_reset_postdata(); ?>

				</div>

				<div class="search-results col-md-12">

	                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article class="post row" id="post-<?php the_ID(); ?>">

                        <!-- <div class="article-holder row"> -->

                            <div class="image col-md-3">

                                <figure class="post-image">

                                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<?php

									        if (has_post_thumbnail()) {
									            the_post_thumbnail('medium');
									        } else {
									            echo '<img src="http://via.placeholder.com/260x195" />';
									        }

								        ?>
                                    </a>

                                </figure>

                            </div>

                            <div class="content col-md-9">

                                <div class="post-title">
                                    <h3 itemprop="headline">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
									</h3>
                                </div>

                                <time class="post-time" datetime="<?php echo esc_attr( get_the_date( 'c' ) ); ?>" itemprop="datePublished" content="<?php echo esc_attr( get_the_date( 'c' ) ); ?>">
                                    <?php echo esc_attr( get_the_date(  ) ); ?>
                                </time>

                            </div>

                        <!-- </div> -->

                    </article>

	                <?php endwhile; endif; ?>

				</div>

				<?php get_template_part('templates/template', 'pagination'); ?>

            </div>

        </div>

    </section>

	<?php else : ?>

	<section class="search default" id="search-no-results">

        <div class="search-container container">

            <div class="search-holder row">

				<div class="search-title main-title col-md-12">

					<h1 class="title -pages"><?php _e('RESULTADO DA BUSCA', 'ps'); ?></h1>
					<h2 class="subtitle"><?php _e('Você procurou por', 'ps'); ?></h2>
					<h6 class="key">"<?php echo $search; ?>"</h6>

					<?php wp_reset_postdata(); ?>

				</div>

				<div class="content col-md-12">

					<p class="no-results center"><?php _e('Infelizmente sua busca não trouxe resultados.', 'ps'); ?></p>

				</div>

            </div>

        </div>

    </section>

	<?php endif; ?>

<?php get_footer(); ?>
